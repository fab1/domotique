#!/bin/bash

# Settings
#crontab -e
# 0 6 * * * sudo /home/pi/domoticz/scripts/sh/tn_hold.sh
# chmod +x /home/pi/domoticz/scripts/sh/tn_hold.sh


DOMO_IP="192.168.1.18"      # Domoticz IP Address
DOMO_PORT="8080"         # Domoticz Port

Tn_IDX="6"               # Idx de la variable Tn
Tn_uservariablename="Tn"  # Nom de la variable Tn

Tn_hold_IDX="7"        # Idx de la variable Tn_hold
Tn_hold_uservariablename="Tn_Hold"  # Nom de la variable Tn_hold

	
        Tn=$(curl -s "http://$DOMO_IP:$DOMO_PORT/json.htm?type=command&param=getuservariable&idx=$Tn_IDX"| jq -r .result[].Value)
		echo "Recuperation valeur variable Tn"
		echo $Tn
		curl -s "http://$DOMO_IP:$DOMO_PORT/json.htm?type=command&param=updateuservariable&idx=$Tn_hold_IDX&vname=$Tn_hold_uservariablename&vtype=2&vvalue=$Tn"
		echo "Mise a jour valeur variable Tn_hold avec la Temperature mini"		
       curl -s "http://$DOMO_IP:$DOMO_PORT/json.htm?type=command&param=updateuservariable&idx=$Tn_IDX&vname=$Tn_uservariablename&vtype=2&vvalue=150"
		echo "Changement valeur variable Tn a 150 pour débuter un nouveau jour"
