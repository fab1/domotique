#!/bin/bash

# Settings
#crontab -e
# 0 18 * * * sudo /home/pi/domoticz/scripts/sh/lance_calcul_dju.sh
# chmod +x /home/pi/domoticz/scripts/sh/lance_calcul_dju.sh
DOMO_IP="192.168.1.18"      # Domoticz IP Address
DOMO_PORT="8080"         # Domoticz Port
nb_IDX="5" # IDX variable Nb Jours de Chauffage 18
uservariablename="Nb Jours de Chauffage"
uservariabletype="2"

urlencode() {
    # urlencode <string>

    local LANG=C
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;; 
        esac
    done
}
       nb=$(curl -s "http://$DOMO_IP:$DOMO_PORT/json.htm?type=command&param=getuservariable&idx=$nb_IDX"| jq -r .result[].Value)
		echo $nb
			if expr "$nb" '>' 0
			then
			echo "test condition avant calcul"
			echo $nb
		nb=$((nb+1))
		echo "test condition apres calcul"
		echo $nb
		uservariablename=$( urlencode "$uservariablename" )
		#curl -s "http://$DOMO_IP:$DOMO_PORT/json.htm?type=command&param=udevices&script=calcul_dju.lua"
		curl -s -i -H "Accept: application/json" "http://$DOMO_IP:$DOMO_PORT/json.htm?type=command&param=updateuservariable&idx=$nb_IDX&vname=$uservariablename&vtype=$uservariabletype&vvalue=$nb"
					else
				echo $nb
		fi
