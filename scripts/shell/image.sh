#!/bin/sh
#############
#
# Script de sauvegarde de l'image complète du serveur Domoticz sur une Freebox V6
# Transmission d un SMS avant et après l'opération
#
# José - JANVIER 2015
#
#############

. ./include_passwd # comprend les identifiants pour acceder à la freebox et transmission des SMS via FreeMobile

# Formatage de la date debut et de l'heure
DATE_DEBUT=`date +%d-%m-%Y`
H_DEPART=`date +%H:%M:%S`
DEBUT_EN_SEC=$(($(echo $H_DEPART | cut -d':' -f1)*3600+$(echo $H_DEPART | cut -d':' -f2)*60+$(echo $H_DEPART | cut -d':' -f3)))

# Transmission d'un premier sms
message=${DATE_DEBUT}' '$H_DEPART' Début de la sauvegarde image du serveur domoticz.'
curl -s -i -k "https://smsapi.free-mobile.fr/sendmsg?user=$user&pass=$pass&msg=$message"

# Montage de la Freebox
echo "Montage de la Freebox"
/sbin/mount.cifs //mafreebox.freebox.fr/Disque\ dur/ /mnt/freebox/ -o user=$user_box,pass=$pass_box

# Sauvegarde sur la Freebox
echo "Sauvegarde sur la Freebox"
dd if=/dev/mmcblk0 | gzip -9 > /mnt/freebox/Backup_img/srv-domoticz-${DATE_DEBUT}'-'$H_DEPART.img.gz

# Démontage de la Freebox
echo "Démontage de la Freebox"
/bin/umount /mnt/freebox

# Formatage de la date de fin et de l'heure
DATE_FIN=`date +%d-%m-%Y`
H_FIN=`date +%H:%M:%S`

# Découpe pour mise en seconde
FIN_EN_SEC=$(($(echo $H_FIN | cut -d':' -f1)*3600+$(echo $H_FIN | cut -d':' -f2)*60+$(echo $H_FIN | cut -d':' -f3)))

# Calcul de la durée d'execution
DUREE_EN_SEC=$(($FIN_EN_SEC-$DEBUT_EN_SEC))

# Remise en Heure - Minute - Seconde
DUREE_H=$(($DUREE_EN_SEC/3600))
DUREE_M=$((($DUREE_EN_SEC%3600)/60))
DUREE_S=$((($DUREE_EN_SEC%3600)%60))

# Transmission sms avec heure de fin et la durée
message=${DATE_FIN}' '$H_FIN' Fin de la sauvegarde image du serveur domoticz. Durée du traitement '$DUREE_H':'$DUREE_M':'$DUREE_S
curl -s -i -k "https://smsapi.free-mobile.fr/sendmsg?user=$user&pass=$pass&msg=$message"
