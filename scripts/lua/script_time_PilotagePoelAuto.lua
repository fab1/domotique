--
-- Domoticz passes information to scripts through a number of global tables
--
-- otherdevices, otherdevices_lastupdate and otherdevices_svalues are arrays for all devices: 
--   otherdevices['yourotherdevicename'] = "On"
--   otherdevices_lastupdate['yourotherdevicename'] = "2015-12-27 14:26:40"
--   otherdevices_svalues['yourotherthermometer'] = string of svalues
--
-- uservariables and uservariables_lastupdate are arrays for all user variables: 
--   uservariables['yourvariablename'] = 'Test Value'
--   uservariables_lastupdate['yourvariablename'] = '2015-12-27 11:19:22'
--
-- other useful details are contained in the timeofday table
--   timeofday['Nighttime'] = true or false
--   timeofday['SunriseInMinutes'] = number
--   timeofday['Daytime'] = true or false
--   timeofday['SunsetInMinutes'] = number
--   globalvariables['Security'] = 'Disarmed', 'Armed Home' or 'Armed Away'
--
-- To see examples of commands see: http://www.domoticz.com/wiki/LUA_commands#General
-- To get a list of available values see: http://www.domoticz.com/wiki/LUA_commands#Function_to_dump_all_variables_supplied_to_the_script
--
-- Based on your logic, fill the commandArray with device commands. Device name is case sensitive. 
--
devicePoel={'ModePoele','Puissance','Comfort/Eco/HorsGel','Ventilateur1','Ventilateur2','BipConfirmation'}
local sensorExt = 'Barometre termometre'
local sensorSalon = 'TempSalon'
local thermostat = 'ConsigneTemp'
local domoticz_url = '192.168.1.18:8080'   --Adresse IP:port de Domoticz
local IDTpsRepos = '563'                     
local IDPoelCOuntStart = '564'        
          
  

local tempoboucle = 10 -- temps entre deux rafaichissement
local TpsStab = 30 --temps en min avant coupure du poel apres atteinte temp
local hysteL = 2
local hysteH = 0.5
local Debug = true --true

local ValeurPoel=0
--IL faut demarer à consigne -1 et s'arreter a consigne+1

--démarage P 3à 4 // ensuite 1 à 5
-- puissance 1 à 5
-- ventilateur 1à 5


-- P1V1  P1V2 P1V3 P2V1 P2V2 P2V3 P2V4 P3V2 P3V3 P3V4 P3V5 P4V3 P4V4 P4V5 P5V3 P5V4 P5V5

commandArray = {}

time= os.date("%H:%M")
local m = tonumber(os.date('%M'))
local h= tonumber(os.date('%H'))

SalonTemp, SalonHum = otherdevices_svalues['TempSalon']:match("([^;]+);([^;]+);([^;]+)")
SalonUpdate= otherdevices_lastupdate['TempSalon']

sWeatherTemp, sWeatherHumidity, sHumFeelsLike, sWeatherPressure = otherdevices_svalues[sensorExt]:match("([^;]+);([^;]+);([^;]+);([^;]+);([^;]+)")
sWeatherTemp = tonumber(sWeatherTemp)
sWeatherHumidity = tonumber(sWeatherHumidity)
sWeatherPressure = tonumber(sWeatherPressure)
poelOnOff = uservariables['PoelOnOff']
TpsRepos = tonumber(uservariables['PoelTime'])	
PoelCOuntStart = tonumber(uservariables['Poelcompteurdémarage'])
PuissanceChauf= tonumber(uservariables['CmdChauff'])	 
consigne = tonumber(otherdevices_svalues[thermostat])     


function log_debug (s)
    if (Debug) then 
        --print ("<font color='#f3031d'>".. s .."</font>");
		print (s)
    end
end   
function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function PuissancePoel(PuissanceChauf)
	local index
	if (PuissanceChauf<7) 		then index = 1
	elseif (PuissanceChauf<15) 	then index = 2
	elseif (PuissanceChauf<20) 	then index = 3
	elseif (PuissanceChauf<25) 	then index = 4
	elseif (PuissanceChauf<30) 	then index = 5
	elseif (PuissanceChauf<40) 	then index = 6
	elseif (PuissanceChauf<50) 	then index = 7
	elseif (PuissanceChauf<55) 	then index = 8
	elseif (PuissanceChauf<60) 	then index = 9
	elseif (PuissanceChauf<65) 	then index = 10
	elseif (PuissanceChauf<70) 	then index = 11
	elseif (PuissanceChauf<75) 	then index = 12
	elseif (PuissanceChauf<80) 	then index = 13
	elseif (PuissanceChauf<85) 	then index = 14
	elseif (PuissanceChauf<90) 	then index = 15
	elseif (PuissanceChauf<95) 	then index = 16
	elseif (PuissanceChauf<=100) 	then index = 17
	end
	return index
end



function envoi_poel(index)
n = os.tmpname ()
-- execute a command   ID MODE USER PUISSANCECHAUF VENTIL1 VENTIL2
if (index == 0) then
--os.execute ("sudo /home/pi/Poel/APPLI_MCZ/src/Appli_Cmd_Arg 1 0 1 0 0 0 > " .. n)
else
Pchauf=1
Ventil1=2
Ventil2=3
--os.execute ('sudo /home/pi/Poel/APPLI_MCZ/src/Appli_Cmd_Arg 1 2 1 '..Pchauf..' '..Ventil1..' '..Ventil2..'> ' .. n)
end
-- display output
for line in io.lines (n) do
  log_debug (line)
end
-- remove temporary file
os.remove (n)
end
  
-- Toutes les dix min si poel en auto
if (m % tempoboucle == 0  and otherdevices['ModePoele'] == 'Auto') then

log_debug('poel auto heure '..h..' minutes '..m)

   if ( h == 0 and m<=(tempoboucle*1.1)) then
	PoelCOuntStart=0	
	log_debug('reset compteur hebdo')
   end



   log_debug('time poel auto')
    --print(sWeatherTemp,sWeatherPressure,SalonTemp, consigne)
        log_debug('temp exterieur '..sWeatherTemp..' - pression exterieur '..sWeatherPressure..' - temp Salon '..SalonTemp..'consigne '..consigne)
	 --calcul du delta
	 deltatemp = consigne - SalonTemp 
		 
	 --delta négatif il fait plus chaud	 
	 if (deltatemp <= 0) then
	 
		if (poelOnOff >= 1) then -- Le poel à demarer on lance un chrono avant arret
			ValeurPoel = 1			
			TpsRepos=TpsRepos + tempoboucle		
			print('tps repos : '..TpsRepos)
			if  (TpsRepos > TpsStab and PoelCOuntStart <=2) 
			then
				poelOnOff = 0		--On coupe le poel	
				 envoi_poel(0)	
			elseif 	(TpsRepos > 2 * TpsStab and PoelCOuntStart <=3) then poelOnOff = 0 end		--On coupe le poel	mais apres plusieur arret on le laisse plus longtemps tourner
		end
		
	 log_debug("cooling")	 	 
	 
	 --delta positif il fait plus froid que consigne
	 else
		TpsRepos=0 -- On reinitalise le temps d'attente stabilisation	 
	 
		if poelOnOff == 0 then -- si le poel est coupé On allume le poel et on lance le démarage
			poelOnOff=31
			log_debug("Démarrage chauffage")
			ValeurPoel = 8
			PoelCOuntStart=PoelCOuntStart+1						
		
		elseif poelOnOff >1 then 
			--On est dans la séquence de démarage on decompte 30 minutes
		
			poelOnOff=poelOnOff - tempoboucle
			if poelOnOff<1 then poelOnOff = 1 end -- Calcul du tempo démarage POel									 
			log_debug("Séquence de Démarrage chauffage")				
			ValeurPoel=PuissancePoel(PuissanceChauf)			
			if ValeurPoel>13 then ValeurPoel=13 end
			
		else 
			log_debug("Regulation auto")
			poelOnOff = 1
			ValeurPoel=PuissancePoel(PuissanceChauf)						
		end
	
	end
	
	log_debug('tps attente '..TpsRepos..' - nb démarage '..PoelCOuntStart..' - etat poel '..poelOnOff..' - Valeur puissancepoel : '..ValeurPoel)

	if (poelOnOff>=1) then
	commandArray['Poel_On_Off'] = 'On'
	 envoi_poel(ValeurPoel)	
	else
	commandArray['Poel_On_Off'] = 'Off'
	
	
	end

	commandArray['Variable:PoelTime'] = tostring(TpsRepos)	
	commandArray['Variable:PoelOnOff'] = tostring(poelOnOff)	
	commandArray['Variable:Poelcompteurdémarage'] = tostring(PoelCOuntStart)	

url = domoticz_url..'/json.htm?type=command&param=udevice&idx='..IDTpsRepos..'&nvalue=0&svalue='..tonumber(TpsRepos)	
 commandArray[1]={['OpenURL']=url} 
url = domoticz_url..'/json.htm?type=command&param=udevice&idx='..IDPoelCOuntStart..'&nvalue=0&svalue='..PoelCOuntStart	
 commandArray[2]={['OpenURL']=url} 	
 


end
return commandArray

