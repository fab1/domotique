-- Version : 20161006_01
-- Author : Snorky
-- Update conso, stock et depense pellet

--------------------------------
-- Variable a editer -----------
--------------------------------
local ajoutSacName = 'IncPellet' -- Nom virtual device switch pour ajouter un sac
local consoIdx = 16 -- IDX virtual device de consommation de pellets
local consoName = 'Pellet' -- Nom virtual device de consommation de pellets
local stock1Name = 'stockPellet' -- Nom user variable du stock de pellets en cours de consommation
--local depenseIdx = 28 -- IDX virtual device de depense de pellets
--local depenseName = 'DepensePellet' -- Nom virtual device de depense de pellets
--local prix1Name = 'prixSac' -- Nom user variable du prix du stock en cours de consommation
--------------------------------
-- Fin des variables a editer --
--------------------------------

--------------------------------
-- Fonctions                  --
--------------------------------

-- TimeDifference
function timedifference(d)
   s = otherdevices_lastupdate[d]
   year = string.sub(s, 1, 4)
   month = string.sub(s, 6, 7)
   day = string.sub(s, 9, 10)
   hour = string.sub(s, 12, 13)
   minutes = string.sub(s, 15, 16)
   seconds = string.sub(s, 18, 19)
   t1 = os.time()
   t2 = os.time{year=year, month=month, day=day, hour=hour, min=minutes, sec=seconds}
   difference = os.difftime (t1, t2)
   return difference
end


commandArray = {}

if (devicechanged[ajoutSacName] and otherdevices[ajoutSacName] == 'On') then     -- Si interrupteur AjoutSac = On
   
   timedifference(consoName)

   if (difference > 60) then    -- On attend 10 minutes avant ajout nouveau sac
   
      -- Update conso --
      consoNew = tonumber(otherdevices_svalues[consoName]) + 1
      commandArray[1] = {['UpdateDevice'] = consoIdx.. '|0|' .. tostring(consoNew)}
      print('Consommation totale : '..consoNew)

      -- Update depense --
      --depenseNew = tonumber(otherdevices_svalues[depenseName]) + tonumber(uservariables[prix1Name])
      --commandArray[2] = {['UpdateDevice'] = depenseIdx .. '|0|' .. tostring(depenseNew)}
      --print('Depense pellet : '..depenseNew..' euros')

      -- Update stock --
      stockNew = tonumber(uservariables[stock1Name]) - 1
      commandArray[2] = {['Variable:'..stock1Name] = tostring(stockNew)}
      print('Nouveau stock de pellet : '..stockNew)


   else
      differencemin = math.floor(difference / 60)
      print('Dernier update de la consommation de pellet il y a moins de 10 minutes ('..differencemin..' min).')
   end

end

return commandArray   
