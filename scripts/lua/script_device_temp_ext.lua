--[[   
~/domoticz/scripts/lua/script_device_temp_ext.lua
 tx = température maximale du jour J mesurée à 2 mètres du sol sous abri et relevée entre J à 6h et J+1 (le lendemain) à 6h UTC. 
 tn = température minimale du jour J mesurée à 2 mètres du sol sous abri et relevée entre J-1 (la veille) à 18h et J à 18h UTC. 
]]--
------------------------------------------------------------------------------

--------------------------------
------ Tableau à éditer ------
--------------------------------
local debugging = true  -- true ou false
local url = '192.168.1.18:8080'   -- user:pass@ip:port de domoticz
local temp_ext  = 'Barometre termometre' -- nom de la sonde extérieure
------------------------------------------------------------------------------
---------------------------------------------------------------------------
function voir_les_logs (s)
    if (debugging) then
        print (s);
    end
end

---------------------------------------------------------------------------

commandArray = {}
if (devicechanged[temp_ext])then
voir_les_logs("=========== Mini/Maxi Température Extérieure (v1.0) ===========",debugging);
      if(uservariables['Tx'] == nil) then
      -- Création de la variable Tx si elle n'existe pas
         commandArray['OpenURL']=url..'/json.htm?type=command&param=saveuservariable&vname=Tx&vtype=2&vvalue=150'
            voir_les_logs("--- --- --- Création Variable Tx manquante --- --- --- ",debugging);
        print('script supendu')
      end
      if(uservariables['Tn'] == nil) then
         commandArray['OpenURL']=url..'/json.htm?type=command&param=saveuservariable&vname=Tn&vtype=2&vvalue=-150'
       -- Création de la variable Tn si elle n'existe pas
            voir_les_logs("--- --- --- Création Variable Manquante Tn --- --- --- ",debugging);
        print('script supendu')
      end
max_min = string.match(otherdevices_svalues[temp_ext], "%d+%.*%d*")
t_max_min = tonumber(max_min)
voir_les_logs("--- --- --- Température Ext : "..t_max_min,debugging);
   if (t_max_min < tonumber(uservariables['Tn'])) then
              voir_les_logs("--- --- --- Température Extérieure inférieure à Variable Tn : "..uservariables['Tn'],debugging);
   commandArray['Variable:Tn'] = tostring(t_max_min) -- mise à jour de la variable tn
voir_les_logs("--- --- --- mise à jour de la Variable Tn  --- --- --- ",debugging);
   elseif (t_max_min > tonumber(uservariables['Tx'])) then
              voir_les_logs("--- --- --- Température Extérieure supérieure à Variable Tx : "..uservariables['Tx'],debugging);
   commandArray['Variable:Tx'] = tostring(t_max_min) -- mise à jour de la variable tx
voir_les_logs("--- --- --- mise à jour de la Variable Tx  --- --- --- ",debugging);   
   end
end
return commandArray
