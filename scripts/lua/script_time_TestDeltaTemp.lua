-- demo time script
-- script names have three name components: script_trigger_name.lua
-- trigger can be 'time' or 'device', name can be any string
-- domoticz will execute all time and device triggers when the relevant trigger occurs
-- 
-- copy this script and change the "name" part, all scripts named "demo" are ignored. 
--
-- Make sure the encoding is UTF8 of the file
--
-- ingests tables: otherdevices,otherdevices_svalues
-- 
-- otherdevices and otherdevices_svalues are two item array for all devices: 
--   otherdevices['yourotherdevicename']="On"
--	otherdevices_svalues['yourotherthermometer'] = string of svalues
--
-- Based on your logic, fill the commandArray with device commands. Device name is case sensitive. 
--
-- Always, and I repeat ALWAYS start by checking for a state.
-- If you would only specify commandArray['AnotherDevice']='On', every time trigger (e.g. every minute) will switch AnotherDevice on.
--
-- The print command will output lua print statements to the domoticz log for debugging.
-- List all otherdevices states for debugging: 
--   for i, v in pairs(otherdevices) do print(i, v) end
-- List all otherdevices svalues for debugging: 
--   for i, v in pairs(otherdevices_svalues) do print(i, v) end

--print('this will end up in the domoticz log')

time= os.date("%H:%M")
local m = tonumber(os.date('%M'))
local h= tonumber(os.date('%H'))
 
json=(loadfile '/home/pi/domoticz/scripts/lua/JSON.lua')()


commandArray = {}


if ((m % 1 == 120000)) then
  print('OK')

  --Get values of previous 8 days
  local t, jresponse, status, decoded_response,idx
  idx=78  -- IDX of the GASMETER

  local config=assert(io.popen('curl http://192.168.1.18:8080/json.htm?type=graph&sensor=temp&idx=78&range=day'))
  local blocjson = config:read('*all')
print(blocjson)
  config:close()
 result=json:decode(blocjson)

printr(result)
--
-- List all data
-- 
  for i = 1, #result do
    record = result[i]
        day = record["d"]
        value = record["te"]
        print(os.date("%X").." "..i.."  Gebruik dag="..day.."   gebruik="..value)

  end
  --
  -- Todays data
  --
--  print(os.date("%X").." vandaag="..result[8]["d"].."   gebruik="..result[8]["te"])
end 

return commandArray
