#ifndef Decode_h
#define Decode_h

#include <core/MySensorsCore.h>

void read_teleinfo();
void init_decode();


// Variables Téléinfo---------------------
struct teleinfo_s {
  char ADCO[13]="";
  char OPTARIF[5]="";
  unsigned int ISOUSC=0; 
  unsigned long BASE=0;
  unsigned long HCHC=0;
  unsigned long HCHP=0;
  unsigned long EJP_HN=0;
  unsigned long EJP_HPM=0;
  unsigned long BBR_HC_JB=0;
  unsigned long BBR_HP_JB=0;
  unsigned long BBR_HC_JW=0;
  unsigned long BBR_HP_JW=0;
  unsigned long BBR_HC_JR=0;
  unsigned long BBR_HP_JR=0;
  char PEJP[3]="";
  char PTEC[5]="";
  char DEMAIN[5]="";
  unsigned int IINST=0;
  unsigned int PAPP=0;
  unsigned int ADPS=0;
  unsigned int IMAX=0;
  char HHPHC[2]="";
};



#endif
