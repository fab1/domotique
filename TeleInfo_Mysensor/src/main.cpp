/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2018 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
*/
//--------------------------------------------------------------------
//                 +/          
//                 `hh-        
//        ::        /mm:       
//         hy`      -mmd       
//         omo      +mmm.  -+` 
//         hmy     .dmmm`   od-
//        smmo    .hmmmy    /mh
//      `smmd`   .dmmmd.    ymm
//     `ymmd-   -dmmmm/    omms
//     ymmd.   :mmmmm/    ommd.
//    +mmd.   -mmmmm/    ymmd- 
//    hmm:   `dmmmm/    smmd-  
//    dmh    +mmmm+    :mmd-   
//    omh    hmmms     smm+    
//     sm.   dmmm.     smm`    
//      /+   ymmd      :mm     
//           -mmm       +m:    
//            +mm:       -o    
//             :dy             
//              `+:     
//--------------------------------------------------------------------
//   __|              _/           _ )  |                       
//   _| |  |   ` \    -_)   -_)    _ \  |   -_)  |  |   -_)     
//  _| \_,_| _|_|_| \___| \___|   ___/ _| \___| \_,_| \___|  
//--------------------------------------------------------------------    
// 2019/10/17 - FB V1.0.1
// 2019/12/15 - FB V1.0.2 - optimisation/simplifications et reduction conso mémoire
// 2019/12/17 - FB V1.0.3 - modif led indication envoi MySensors
// 2019/12/22 - FB V1.0.4 - remove warning message
// 2019/12/22 - FB V1.0.5 - optimisation
// 2020/04/25 - FB V1.0.6 - ADCO bug fix
// 2020/04/25 - FB V1.1.0 - Mode standard
// 2020/08/18 - FB V1.1.1 - Correction sur NTARF et LTARF - Merci David MARLINGE
//                        - Ajout EASF01..10, EASD01..04 et ERQ1..4
//                        - Optimisation mémoire
// 2020/08/26 - FB V1.1.2 - Ajout delais sur presentation et send, afin de soulager la gateway
//                        - Envoi données producteur et triphasé si besoin
//                        - Correction sur send EASF et EADS en WH au lieu du VA
//                        - Accents retirés sur les libellés de présentation
//--------------------------------------------------------------------
// Enable debug prints
//#define MY_DEBUG


//#define MY_NODE_ID 2

#define VERSION   "v1.1.2"

// Set LOW transmit power level as default, if you have an amplified NRF-module and
// power your radio separately with a good regulator you can turn up PA level.
//#define MY_RF24_PA_LEVEL RF24_PA_LOW

#define MY_BAUD_RATE 9600    // mode standard

// Enable and select radio type attached
#define MY_RADIO_RF24
//#define MY_RADIO_NRF5_ESB
//#define MY_RADIO_RFM69
//#define MY_RADIO_RFM95

#define MY_DEFAULT_ERR_LED_PIN 4  // Error led pin
#define MY_DEFAULT_RX_LED_PIN  13 // Receive led pin, on board LED
#define MY_DEFAULT_TX_LED_PIN  3  // 

#define GW_DELAY	25 // ms

#include <MySensors.h>
#include <Decode.h>

//#define MY_RADIO_RFM69
#define MY_RF24_PA_LEVEL RF24_PA_HIGH

// Enabled repeater feature for this node
#define MY_REPEATER_FEATURE


uint32_t SEND_FREQUENCY_CONSO =    5000; // Minimum time between send (in milliseconds). We don't want to spam the gateway.
uint32_t SEND_FREQUENCY_INFO =    90000; // Minimum time between send (in milliseconds). We don't want to spam the gateway.
uint32_t lastSend_conso = 0;
uint32_t lastSend_info = 0;

unsigned long startTime; // Date démarrage 


#define CHILD_ID_ADCO     0
#define CHILD_ID_OPTARIF  1
#define CHILD_ID_ISOUSC   2
#define CHILD_ID_BASE     3
#define CHILD_ID_HCHC     4
#define CHILD_ID_HCHP     5
#define CHILD_ID_EJP_HN   6
#define CHILD_ID_EJP_HPM  7
#define CHILD_ID_BBR_HC_JB 8
#define CHILD_ID_BBR_HP_JB 9
#define CHILD_ID_BBR_HC_JW 10
#define CHILD_ID_BBR_HP_JW 11
#define CHILD_ID_BBR_HC_JR 12
#define CHILD_ID_BBR_HP_JR 13
#define CHILD_ID_PEJP      14
#define CHILD_ID_PTEC      15
#define CHILD_ID_DEMAIN    16
#define CHILD_ID_IINST     17
#define CHILD_ID_PAPP      18
#define CHILD_ID_ADPS      19
#define CHILD_ID_IMAX      20
#define CHILD_ID_HHPHC     21


MyMessage msgTEXT( 0, V_TEXT);        // S_INFO
MyMessage msgCURRENT( 0, V_CURRENT ); // S_MULTIMETER
MyMessage msgVOLTAGE( 0, V_VOLTAGE ); // S_MULTIMETER
MyMessage msgWATT( 0, V_WATT ); // S_POWER
MyMessage msgKWH( 0, V_KWH );   // S_POWER  
MyMessage msgVA( 0, V_VA );     // S_POWER
MyMessage msgVAR1( 0, V_VAR1 ); // Custom value


teleinfo_s teleinfo;

boolean mode_producteur = false;
boolean mode_triphase = false;


//-----------------------------------------------------------------------
// This is called when a new time value was received
void receiveTime(unsigned long controllerTime) 
{
  // incoming time 
  Serial.print(">> Time received from gw: ");
  Serial.println(controllerTime);
  startTime = controllerTime;
}

//--------------------------------------------------------------------
void setup()
{  
   init_decode();
   Serial.begin(MY_BAUD_RATE); 
  //Serial.println(F("   __|              _/           _ )  |"));
  //Serial.println(F("   _| |  |   ` \\    -_)   -_)    _ \\  |   -_)  |  |   -_)"));
  //Serial.println(F("  _| \\_,_| _|_|_| \\___| \\___|   ___/ _| \\___| \\_,_| \\___|"));
  //Serial.print(F("                                             "));
  Serial.println(VERSION);
  memset(&teleinfo, 0, sizeof(teleinfo)); 
}

//--------------------------------------------------------------------
void presentation()
{
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("Teleinfo", VERSION);
  
  // Récupere l'heure de la gateway
  requestTime();
  
   // Register this device as power sensor
  present( CHILD_ID_ADCO, S_INFO, F("Ident Compteur"));
  wait(GW_DELAY);
  present( CHILD_ID_OPTARIF, S_INFO,F("Option Tarif") );
  wait(GW_DELAY);
  present( CHILD_ID_ISOUSC, S_MULTIMETER,F("Intensite souscrite") );
  wait(GW_DELAY);
  present( CHILD_ID_BASE, S_POWER, F("Index si option Base") );
  wait(GW_DELAY);
  present( CHILD_ID_HCHC, S_POWER ,F("Index heures creuse"));
  wait(GW_DELAY);
  present( CHILD_ID_HCHP, S_POWER , F("Index  heures pleine"));
  wait(GW_DELAY);
  present( CHILD_ID_EJP_HN, S_POWER, F("Index  heures normal EJP") );
  wait(GW_DELAY);
  present( CHILD_ID_EJP_HPM, S_POWER ,F("Index  heures pointe EJP"));
  wait(GW_DELAY);
  present( CHILD_ID_BBR_HC_JB, S_POWER, F("Index  heures creuse jours bleu"));
  wait(GW_DELAY);
  present( CHILD_ID_BBR_HP_JB, S_POWER,F("Index  heures pleine jours blanc") );
  wait(GW_DELAY);
  present( CHILD_ID_BBR_HC_JW, S_POWER, F("Index  heures creuses jours blanc") );
  wait(GW_DELAY);
  present( CHILD_ID_BBR_HP_JW, S_POWER,F("Index  heures pleine jours blanc") );
  wait(GW_DELAY);
  present( CHILD_ID_BBR_HC_JR, S_POWER,F("Index  heures creuse jours rouge") );
  wait(GW_DELAY);
  present( CHILD_ID_BBR_HP_JR, S_POWER ,F("Index  heures pleine jours rouge"));
  wait(GW_DELAY);
  present( CHILD_ID_PEJP, S_INFO, F("Preavis EJP") );
  wait(GW_DELAY);
  present( CHILD_ID_PTEC, S_INFO, F("Index tarifaire en cours"));
  wait(GW_DELAY);
  present( CHILD_ID_DEMAIN, S_INFO, F("Couleur lendemain") );
  wait(GW_DELAY);
  present( CHILD_ID_IINST, S_MULTIMETER, F("Intensité Instantanée") );
  wait(GW_DELAY);
  present( CHILD_ID_ADPS, S_MULTIMETER,F("Depassement Puissance") );
  wait(GW_DELAY);
  present( CHILD_ID_IMAX, S_MULTIMETER,F("Intensite maximale"));
  wait(GW_DELAY);
  present( CHILD_ID_PAPP, S_POWER ,F("Puissance Apparente") );
  wait(GW_DELAY);
  present( CHILD_ID_HHPHC, S_INFO,F("Groupe Horaire") );

}


//--------------------------------------------------------------------
void  send_teleinfo_info()
{
    //-- ADCO
  send(msgTEXT.setSensor(CHILD_ID_ADCO).set(teleinfo.ADCO));
  
  //--OPTARIF
  send(msgTEXT.setSensor(CHILD_ID_OPTARIF).set(teleinfo.OPTARIF));

  // --ISOUC
  send(msgCURRENT.setSensor(CHILD_ID_ISOUSC).set(teleinfo.ISOUSC));

    //-- PTEC
  send(msgTEXT.setSensor(CHILD_ID_PTEC).set(teleinfo.PTEC));

  
  // --IMAX
  send(msgCURRENT.setSensor(CHILD_ID_IMAX).set(teleinfo.IMAX));

}


void send_teleinfo_conso()
{
boolean flag_hhphc = false;

  // BASE -------
  if (strcmp(teleinfo.OPTARIF, "BASE" ) == 0) {
    send(msgKWH.setSensor(CHILD_ID_BASE).set(teleinfo.BASE));
  }

  // HC
  if (strcmp(teleinfo.OPTARIF, "HC.." ) == 0) {
      Serial.println("MODE HC");
      send(msgKWH.setSensor(CHILD_ID_HCHC).set(teleinfo.HCHC));
      send(msgKWH.setSensor(CHILD_ID_HCHP).set(teleinfo.HCHP));
  }

  // EJP
  if (strcmp(teleinfo.OPTARIF, "EJP." ) == 0) {
      send(msgKWH.setSensor(CHILD_ID_EJP_HN).set(teleinfo.EJP_HN));
      send(msgKWH.setSensor(CHILD_ID_EJP_HPM).set(teleinfo.EJP_HPM));
      send(msgKWH.setSensor(CHILD_ID_PEJP).set(teleinfo.PEJP));

      flag_hhphc = true;
  }
  
  // BBR - TEMPO
  if (strstr(teleinfo.OPTARIF, "BBR") != NULL) {
      send(msgKWH.setSensor(CHILD_ID_BBR_HC_JB).set(teleinfo.BBR_HC_JB));
      send(msgKWH.setSensor(CHILD_ID_BBR_HP_JB).set(teleinfo.BBR_HP_JB));
      send(msgKWH.setSensor(CHILD_ID_BBR_HC_JW).set(teleinfo.BBR_HC_JW));
      send(msgKWH.setSensor(CHILD_ID_BBR_HP_JW).set(teleinfo.BBR_HP_JW));
      send(msgKWH.setSensor(CHILD_ID_BBR_HC_JR).set(teleinfo.BBR_HC_JR));
      send(msgKWH.setSensor(CHILD_ID_BBR_HP_JR).set(teleinfo.BBR_HP_JR));
      send(msgTEXT.setSensor(CHILD_ID_DEMAIN).set(teleinfo.DEMAIN));

      flag_hhphc = true;
  }
  

  // IINST
  send(msgCURRENT.setSensor(CHILD_ID_IINST).set(teleinfo.IINST));

  // PAPP
  send(msgWATT.setSensor(CHILD_ID_PAPP).set(teleinfo.PAPP));

  // ADPS
  send(msgCURRENT.setSensor(CHILD_ID_ADPS).set(teleinfo.ADPS));


  // HHPHC
  if (flag_hhphc == true) { // cas particulier, appartient a EJP et TEMPO
      send(msgTEXT.setSensor(CHILD_ID_HHPHC).set(teleinfo.HHPHC));
  }
}


//--------------------------------------------------------------------
void loop()
{
  uint32_t currentTime = millis();

  // lecture teleinfo -------------------------
  //Serial.println(F("loop"));
  read_teleinfo();
 
  // Envoi les infos de consommation ---------- 
  if (currentTime - lastSend_conso > SEND_FREQUENCY_CONSO) {
    send_teleinfo_conso();
    lastSend_conso = currentTime;
  } 

   // Envoi les infos techniques ---------- 
  if (currentTime - lastSend_info > SEND_FREQUENCY_INFO) {
    send_teleinfo_info();
    lastSend_info = currentTime;
  } 

}